import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import messaging
from robot.libraries.BuiltIn import BuiltIn

from RobinFirestoreLibrary.firestoreconfig import read_firestore_config
from RobinFirestoreLibrary.firestoreconfig import read_firestore_key_config
from RobinFirestoreLibrary.firestoreconfig import read_messaging_config


class RobinFirestoreUtils:
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    def init(self):
        db = None
        if not len(firebase_admin._apps):
            key = read_firestore_key_config()
            cred = credentials.Certificate(key)
            firebase_admin.initialize_app(cred)
            db = firestore.client()
        else:
            db = firestore.client()
        return db

    def add(self, document, data):
        env = BuiltIn().get_variable_value("${env}")
        db = self.init()
        firestore = read_firestore_config(env)
        doc_ref = db.collection(firestore['collection']).document(document)
        doc_ref.add(data)

    def update(self, document, data):
        env = BuiltIn().get_variable_value("${env}")
        db = self.init()
        firestore = read_firestore_config(env)
        doc_ref = db.collection(firestore['collection']).document(document)
        doc_ref.update(data)

    def delete(self, document):
        env = BuiltIn().get_variable_value("${env}")
        db = self.init()
        firestore = read_firestore_config(env)
        doc_ref = db.collection(firestore['collection']).document(document)
        doc_ref.delete()

    def get(self, document):
        env = BuiltIn().get_variable_value("${env}")
        db = self.init()
        firestore = read_firestore_config(env)
        doc_ref = db.collection(firestore['collection']).document(document)
        doc = doc_ref.get()
        if doc.exists:
            return doc.to_dict()
        else:
            raise Exception('No such document!')

    def subscribe_to_topic(self, tokens):
        """
        Subscribes a list of registration tokens to an FCM topic.
        :param tokens: A non-empty list of device registration tokens.
        List may not have more than 1000 elements.
        :return: A promise fulfilled with the server's response after the device has been
        subscribed to the topic.
        """
        env = BuiltIn().get_variable_value("${env}")
        self.init()
        config = read_messaging_config(env)
        messaging.subscribe_to_topic(tokens, config['notification_topic'])

    def unsubscribe_from_topic(self, tokens):
        """
        Unsubscribes a list of registration tokens from an FCM topic.
        :param tokens: A non-empty list of device registration tokens.
        List may not have more than 1000 elements.
        :return: A promise fulfilled with the server's response after the device has been
        subscribed to the topic.
        """
        env = BuiltIn().get_variable_value("${env}")
        self.init()
        config = read_messaging_config(env)
        messaging.unsubscribe_from_topic(tokens, config['notification_topic'])
