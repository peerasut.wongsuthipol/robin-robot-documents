def read_firestore_key_config():
    firestore_key = {}

    firestore_key['type'] = 'service_account'
    firestore_key['project_id'] = 'robinhood-f74c9'
    firestore_key['private_key_id'] = '094803f8c7fd81e3c490dd4226df89843a47e1fc'
    firestore_key['private_key'] = '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCtQIWWF/iamzII\nodrlncnBNXIUlHdMr3qjON4n8hIpiqodkGDpb5i2L62Xqmo9r9aMXQjzzngojn+w\n8yV0/wHVX0bczC3oogoO+/DTAH2Z1BP1F9JJJR+sgHEG8H4tVd4EYV1Vl1x7Pj2S\nkKHtkKGkq7xVQok7ZXfi/3HAYGOoroQ6nzC4JLXUC/h9krPRQmPGkFxRXpvCNTCP\nm/3+6KnGqwMD0dWNNepnEmz8+KbXoBTINHhYk3vhlbNhv8fXi4wcz/7pb/A0pAp4\ndyWC+HJD+p1HTjf71C2IlHnCWVcsJzksKvUr3bMcwaZtCY4/3EqTkJ41G4XeWrxM\nNGrxt5k5AgMBAAECggEAJR38IKZdD/5szzWh2GygE0EYgjk2f9reasadrbAqXPUE\nshXC9W//pxR629HaNgIMdIDgEJ+s1X+F8sp8vV5+rZBedaJWuY6xmfk25HWn8wNh\nDTK/zgTijsnrrSFr3tmVGZokbVhiq1KvzU0oK+W/A7D5Rcjd7rgz4ZF5dM4Ru8rh\nLvG+YNac9VSMiNakG9HpuBEyOnzK4RE/6GPg+HTaWZrtvO5BzFxl8aRkwm1hH1G4\nemcS9JN2PiA1Nh+lyB7gsyj761i00lS9oHRbfd3ksTFfGqAHxv3zNUvP/ud5ZdSO\nEHzwZlnY1MCUc2TrvPO3v2mjVWIPBo5qJEsoG7ZAcwKBgQDjbnsOJeO2jbkvgj/d\n0VARRZkOXfUYceF1HFYdLxV7G5gkaN56p3trL8ZlypVhSPaoSnuHD5YniC0m6iiJ\nQ/Ju5bLbmXe2sPQt9FuaBKpvvb1clNAoy2t+s6npb3UOFE9jAgoiTPH0cIazY59a\nA/qeRhwzY2RtXNp/Lg/839q6MwKBgQDDA8oc7TyPp0uYOpRkvxUXgujNzAv68V4P\nyMobMAskjxAS2rrIebHP/xZzy7AkGJ2Fd9BiMvvfyMa5lnPiIWlGKnTv9lov9atS\neH5VBK8Nw2/jrorjVEvfBqx0+sENzZcX3cS7JPeqdbAerYg7OC5ZIWfuq6fmdErQ\nAgMzwQeK4wKBgQC1LuTVppW8bzdig89BdWjl+Z4vdea2LLNCSurHoSTpyFfyWaEz\n8Y0/HPZfTX6n3Cq0c3HpbR9gKvpdkBnvELRoaJGOpLpcTqiYm50GHEVzc6MRyHP0\nq+ndLGBmWabVKpT7+Tn9jXMMG0EUVH80hQr5YD1DKyFshpaCzz1pZSSpRQKBgDIv\n7XnYS/ekVD4PBVWQH3t3gzZDRuoD5/Mj929bw+JatwZDKEKjnvEqcrrGNsRzWUaZ\n+RTsjgmfgBUO9S/C7XbTQljnC3rAPKDyxYBXvj1KiKcNqkyqywPQkd1hEwHcYQAR\nYKnQ77P8qaCbhC3JXZQulrLDlmgmkYJKokxVZv2FAoGAfapElABEqq8/TfovnM1K\n6xkkBRXe0I8EmA46SPiBI6Jh0CwAK0t0Ek4iNu0eOySN12c6Th0HHRoJMHf/0op2\nRjP7WskJtDgjLQGBzfh8OkK3OZLW3T1GiGjVWa/mSKGpOOljHHGouesgC1ytkjbC\nEvoMBLHB/mLM2BBZ3AhFeMA=\n-----END PRIVATE KEY-----\n'
    firestore_key['client_email'] = 'firebase-adminsdk-xxnp4@robinhood-f74c9.iam.gserviceaccount.com'
    firestore_key['client_id'] = '106356978751533210363'
    firestore_key['auth_uri'] = 'https://accounts.google.com/o/oauth2/auth'
    firestore_key['token_uri'] = 'https://oauth2.googleapis.com/token'
    firestore_key['auth_provider_x509_cert_url'] = 'https://www.googleapis.com/oauth2/v1/certs'
    firestore_key['client_x509_cert_url'] = 'https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-xxnp4%40robinhood-f74c9.iam.gserviceaccount.com'

    return firestore_key


def read_firestore_config(env=''):
    firestore = {}
    if (env == 'dev' or env == 'dev2' or env == 'qa'):
        firestore['collection'] = 'order-status-updates-' + env.lower()
    else:
        raise Exception('env {0} not found in config'.format(env))

    return firestore


def read_messaging_config(env=''):
    config = {}
    if (env == 'dev' or env == 'dev2' or env == 'qa'):
        config['notification_topic'] = 'RBH_GLOBAL_MSG_' + env.upper()
    else:
        raise Exception('env {0} not found in config'.format(env))
    return config
