from __future__ import absolute_import

from .RobinFirestoreUtils import RobinFirestoreUtils


class RobinFirestoreLibrary(RobinFirestoreUtils):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
