import requests
from importlib.metadata import version
import urllib.parse


# Get version
latest_version = version('robin-robot-library')

# Get changelog
f = open("robin_robot_library.egg-info/PKG-INFO", "r")
change_log = ""
is_found = False
for line in f:
    if line == "\n":
        is_found = True
    if is_found is True and line != "\n":
        change_log = change_log + line

print(change_log)
f.close()

url = "https://notify-api.line.me/api/notify"

msg = f"""
*Robin Library V{latest_version} is LIVE!!* มา Upgrade กันได้จ้า

Change Log:

{change_log}

*Upgrading Library* :
```pip3 install -U git+https://gitlab.com/scbtechx/pv-robinhood/robinhood-qa/robin-robot-library.git```

Documentation: https://scbtechx.gitlab.io/pv-robinhood/robinhood-qa/robin-robot-library
"""

encoded_msg = urllib.parse.quote(msg)
payload = f'message={encoded_msg}&stickerPackageId=6370&stickerId=11088036&notificationDisabled=true'
headers = {
  'Content-Type': 'application/x-www-form-urlencoded',
  'Authorization': 'Bearer FbCr1Saqw6Olv05ohbP5gBcaxjsReNBGk3W8So4TIye'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
if response.json()['status'] != 200:
    raise Exception(response.text)
