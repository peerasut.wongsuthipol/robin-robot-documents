from configparser import ConfigParser
from io import StringIO


def read_request_config(env='', ms=''):
    request = {}
    if (env == 'dev'):
        request['host'] = 'https://apis-' + env.lower() + '.alp-robinhood.com/' + ms.lower()
        request['lb_host'] = 'http://' + ms.lower() + '-' + env.lower() + '.alp-robinhood.com'
        request['b2b_host'] = 'https://b2b-apis-' + env.lower() + '.alp-robinhood.com/' + ms.lower()
        request['b2b_api_key'] = 'XqA3r552XW29WPrjXXK6V85nKPxY8oee7NuZhJQn'
        request['api_key'] = 'C4NGGqfTSaaf4wADNRiOF7mOmnMA24PK3bOFskIf'
        request['b2b_oper_host'] = 'https://b2b-oper-apis-' + env.lower() + '.alp-robinhood.com'
        request['b2b_oper_api_key'] = '3SWn1ALjhU7XPruV6B8IoO5dBec9T8ja9MGWzY5h'
        request['promotion_host'] = 'https://promotion-' + env.lower() + '.alp-robinhood.com/'
    elif (env == 'dev2'):
        request['host'] = 'https://apis-' + env.lower() + '.alp-robinhood.com/' + ms.lower()
        request['lb_host'] = 'http://' + ms.lower() + '-' + env.lower() + '.alp-robinhood.com'
        request['b2b_host'] = 'https://b2b-apis-' + env.lower() + '.alp-robinhood.com/' + ms.lower()
        request['b2b_api_key'] = '173wGjkXnP6qKsKhCBUUWNUKeMr8YkG3B5Rez1Ya'
        request['api_key'] = 'uClP7EaDLb3ID22rggifL8Qwyo1etXkIaLLknyyf'
        request['b2b_oper_host'] = 'https://b2b-oper-apis-' + env.lower() + '.alp-robinhood.com'
        request['b2b_oper_api_key'] = 'lPjzVvpazhaEFyJP0SJzP3cHVZ7We5zg4KjKRmGf'
        request['promotion_host'] = 'https://promotion-' + env.lower() + '.alp-robinhood.com/'    
    elif (env == 'qa'):
        request['host'] = 'https://apis-' + env.lower() + '.alp-robinhood.com/' + ms.lower()
        request['lb_host'] = 'http://' + ms.lower() + '-' + env.lower() + '.alp-robinhood.com'
        request['b2b_host'] = 'https://b2b-apis-' + env.lower() + '.alp-robinhood.com/' + ms.lower()
        request['b2b_api_key'] = 'fyPolaGMdSresjzU4zxIibHZSvYNvl6KY3wDCV10'
        request['api_key'] = 'e5FDnHGrYh313tfMeytCs99KrosK6uBa5EHKiGcw'
        request['b2b_oper_host'] = 'https://b2b-oper-apis-' + env.lower() + '.alp-robinhood.com'
        request['b2b_oper_api_key'] = 'osYLFuxxFx6ZmM8B4EWSg4Xi9smVnMIw3wFyUbQc'
        request['promotion_host'] = 'https://promotion-' + env.lower() + '.alp-robinhood.com/'
    elif (env == 'local'):
        request['host'] =  'http://localhost:8000'
        request['lb_host'] =  'http://localhost:8000'
        request['api_key'] = 'hlt0KHNvEE18Oj01wUkkcaVhG0zCq2EV15U7h5PD'
        request['promotion_host'] = 'https://promotion-' + env.lower() + '.alp-robinhood.com/'
        request['promotion_engine_host'] = 'https://promotion-engine-' + env.lower() + '.alp-robinhood.com/'
    else:
        raise Exception('env {0} not found in config'.format(env))

    return request
