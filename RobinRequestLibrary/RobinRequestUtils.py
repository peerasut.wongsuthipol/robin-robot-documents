import json
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from RobinRequestLibrary.requestconfig import read_request_config
from robot.libraries.BuiltIn import BuiltIn
import logging

class RobinRequestUtils:
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

    def request(self, method, ms, uri, header, body=None):
        env = BuiltIn().get_variable_value("${env}")
        req_config = read_request_config(env, ms)
        header = json.dumps(header)
        headers = {
            'Content-Type': 'application/json',
            'x-api-key': req_config['api_key']
        }
        headers.update(json.loads(header))
        payload = None
        if body is not None:
            payload = json.dumps(body)
        url = req_config['host'] + uri
        if method == 'GET':
            response = requests.get(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'POST':
            response = requests.post(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'PUT':
            response = requests.put(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'DELETE':
            response = requests.delete(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'PATCH':
            response = requests.patch(url, data=payload, headers=headers, verify=False)
            return response

    def request_b2b(self, method, ms, uri, header, body=None):
        env = BuiltIn().get_variable_value("${env}")
        req_config = read_request_config(env, ms)
        header = json.dumps(header)
        headers = {
            'Content-Type': 'application/json',
            'x-api-key': req_config['b2b_api_key']
        }
        headers.update(json.loads(header))
        payload = None
        if body is not None:
            payload = json.dumps(body)
        url = req_config['b2b_host'] + uri
        if method == 'GET':
            response = requests.get(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'POST':
            response = requests.post(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'PUT':
            response = requests.put(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'DELETE':
            response = requests.delete(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'PATCH':
            response = requests.patch(url, data=payload, headers=headers, verify=False)
            return response
    
    def request_loadbalance(self, method, ms, uri, header, body=None):
        env = BuiltIn().get_variable_value("${env}")
        req_config = read_request_config(env, ms)
        header = json.dumps(header)
        headers = {
            'Content-Type': 'application/json'
        }
        headers.update(json.loads(header))
        payload = None
        if body is not None:
            payload = json.dumps(body)
        url = req_config['lb_host'] + uri
        if method == 'GET':
            response = requests.get(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'POST':
            response = requests.post(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'PUT':
            response = requests.put(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'DELETE':
            response = requests.delete(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'PATCH':
            response = requests.patch(url, data=payload, headers=headers, verify=False)
            return response
    
    def request_b2b_oper(self, method, ms, uri, header, body=None):
        env = BuiltIn().get_variable_value("${env}")
        req_config = read_request_config(env, ms)
        header = json.dumps(header)
        headers = {
            'Content-Type': 'application/json',
            'x-api-key': req_config['b2b_oper_api_key']
        }
        headers.update(json.loads(header))
        payload = None
        if body is not None:
            payload = json.dumps(body)
        url = req_config['b2b_oper_host'] + uri
        if method == 'GET':
            response = requests.get(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'POST':
            response = requests.post(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'PUT':
            response = requests.put(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'DELETE':
            response = requests.delete(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'PATCH':
            response = requests.patch(url, data=payload, headers=headers, verify=False)
            return response
    
    def request_other(self, method, uri, header=None, body=None):
        headers = {}
        if header is not None:
            header = json.dumps(header)
            headers.update(json.loads(header))
        payload = None
        if body is not None:
            payload = json.dumps(body)
        if method == 'GET':
            response = requests.get(uri, data=payload, headers=headers, verify=False)
            return response
        if method == 'POST':
            response = requests.post(uri, data=payload, headers=headers, verify=False)
            return response
        if method == 'PUT':
            response = requests.put(uri, data=payload, headers=headers, verify=False)
            return response
        if method == 'DELETE':
            response = requests.delete(uri, data=payload, headers=headers, verify=False)
            return response
        if method == 'PATCH':
            response = requests.patch(uri, data=payload, headers=headers, verify=False)
            return response

    def custom_request(self, method, uri, headers=None, body=None):
        payload = None
        if body is not None:
            payload = json.dumps(body)
        response = requests.request(method, uri, headers=headers, data=payload)
        return response

    def request_omise_token(self, body=None):
        body = json.dumps(body)
        body_json = json.loads(body)
        payload = 'card[name]='+ body_json['name'] +'&card[number]='+ body_json['number'] +'&card[expiration_month]='+ body_json['expirationMonth'] +'&card[expiration_year]='+ body_json['expirationYear'] +'&card[city]='+ body_json['city'] +'&card[postal_code]='+ body_json['postalCode'] +'&card[security_code]='+ body_json['securityCode']
        
        headers = {
            'Authorization': 'Basic cGtleV90ZXN0XzVqdTkyYmhseDM1eGY3Mng4a206',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        url = "https://vault.omise.co/tokens"
        response = requests.request("POST", url, headers=headers, data = payload)
        return response
    
    def request_promotion(self, method, ms, uri, header, body=None):
        env = BuiltIn().get_variable_value("${env}")
        req_config = read_request_config(env, ms)
        header = json.dumps(header)
        headers = {
            'Content-Type': 'application/json'
        }
        headers.update(json.loads(header))
        payload = None
        if body is not None:
            payload = json.dumps(body)
        url = req_config['promotion_host'] + uri
        if method == 'GET':
            response = requests.get(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'POST':
            response = requests.post(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'PUT':
            response = requests.put(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'DELETE':
            response = requests.delete(url, data=payload, headers=headers, verify=False)
            return response
        if method == 'PATCH':
            response = requests.patch(url, data=payload, headers=headers, verify=False)
            return response

    def request_file(self, uri, header, file_path):
        header = json.dumps(header)
        headers = { }
        headers.update(json.loads(header))
        files = {'file': open(file_path, 'rb')}
        response = requests.post(uri, headers=headers, files = files)
        return response
