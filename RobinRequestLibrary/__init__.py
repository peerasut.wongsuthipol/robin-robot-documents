from __future__ import absolute_import

from .RobinRequestUtils import RobinRequestUtils


class RobinRequestLibrary(RobinRequestUtils):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'