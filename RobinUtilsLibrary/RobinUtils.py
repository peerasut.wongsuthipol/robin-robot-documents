from random import choice
from string import ascii_lowercase
from robot.api import logger
import re


class RobinUtils:
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    @staticmethod
    def generate_correlationid(prefix: str):
        """Randomly generates 4 lowercase characters with an underscore in front of them, then concatenates
        with the given ``prefix``.

        Returns ``correlationId`` which can be used in request's header.

        If the ``prefix`` has more than 31 characters, it gives warning in logging file.
        (The whole correlationId should be less than or equal to 35 characters.)

        Example:
        | ${correlationId} | Generate correlationid | robot_landing |
        =>
        ``${correlationId} = robot_landing_aeod``
        """
        if len(prefix) > 31:
            msg = '<b>This prefix, ' + prefix + ', is longer than 31 characters.</b> <p>CorrelationId should be less than or equal to 36 characters. Suffix is always 5 characters. So that prefix should be less than or equal to 31 characters.</p>\n'
            logger.warn(msg, html=True)
        random_char = ''.join(choice(ascii_lowercase) for i in range(4))
        return prefix + '_' + random_char

    @staticmethod
    def date_time_format_should_match(time: str):
        """Fails when given time format is not ``dd/mm/yyyy HH:mm:ss.ms`` (``ms`` must be 3 digits)

        Should be used when to verify timestamp in Audit Log. [https://scbtechx.atlassian.net/wiki/spaces/ROBIN/pages/9829652929/All+Event+Log|Event log reference]
        """
        time_pattern = re.compile("^([0-2][0-9]|3[01])/(0[0-9]|1[0-2])/[0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}(\.)[0-9]{3}$")
        if not time_pattern.match(time):
            raise AssertionError("timestamp: '%s' should be format as dd/mm/yyyy HH:mm:ss.ms" % time)

    @staticmethod
    def date_time_with_T_format_should_match(time: str):
        """
        Fails when given time format is not ``yyyy-mm-ddTHH:mm:ss+07:00``

        Should be used when to verify timestamp in Audit Log. [https://scbtechx.atlassian.net/wiki/spaces/ROBIN/pages/9829652929/All+Event+Log|Event log reference]
        """
        time_pattern = re.compile("^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])T([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])\+07:00$")
        if not time_pattern.match(time):
            raise AssertionError("TIMESTAMP: '%s' should be format as yyyy-mm-ddTHH:mm:ss+07:00" % time)
