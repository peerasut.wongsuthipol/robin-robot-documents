from __future__ import absolute_import

from .RobinUtils import RobinUtils


class RobinUtilsLibrary(RobinUtils):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
