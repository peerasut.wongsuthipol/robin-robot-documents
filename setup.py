from setuptools import setup

long_description = '''
V 2.1.0
Add keyword `s3_copy`, `s3_upload_file` for AWSLib:
- s3_copy: copy a file to another destination (you can rename the destination file)
- s3_upload_file: upload a file from disk to S3 (you can rename the destination file)
'''

setup(name='robin-robot-library',
      version='2.1.0',
      description='Robot-Framework-Robinhood-App',
      long_description=long_description,
      author='Robinhood Dev BE & QA',
      author_email='robinhooddevice@gmail.com',
      url='',
      download_url='',
      license='',
      # packages=['RobinRequestLibrary',
      #           'RobinElasticLibrary',
      #           'RobinDatabaseLibrary',
      #           'RobinUtilsLibrary',
      #           'RobinFirestoreLibrary',
      #           'RobinAWSLibrary'],
      packages=['RobinAWSLibrary'],
      install_requires=['numpy>=1.9.1',
                        'scipy>=0.14',
                        'six>=1.9.0',
                        'pyyaml>=5',
                        'fmf>=0.7',
                        'h5py',
                        'mysql-connector-python',
                        'setupcfg',
                        'robotframework',
                        'robotframework-databaselibrary',
                        'robotframework-httplibrary',
                        'requests',
                        'pymysql',
                        'boto3',
                        'firebase_admin'],
      # package_data={'RequestsLibrary': ['tests/*.txt']}
      )
