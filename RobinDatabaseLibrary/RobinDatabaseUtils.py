from mysql.connector import MySQLConnection, Error
from robot.api import logger
from RobinDatabaseLibrary.dbconfig import read_db_config
from robot.libraries.BuiltIn import BuiltIn
import random

class RobinDatabaseUtils:
    ROBOT_LISTENER_API_VERSION = 2
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    def __init__(self):
        """
            For error priorities, only Critical and Low are being used.
        """
        self.ROBOT_LIBRARY_LISTENER = self
        self.suite = []
        self.suite_attr = []
        self.keywords = []
        self.keywords_attr = []
        self.error_priority = {
            1: "Critical",
            2: "High",
            3: "Medium",
            4: "Low"
        }

    def _start_suite(self, name, attrs):
        self.suite.append(name)
        self.suite_attr.append(attrs)

    def _end_suite(self, name, attrs):
        self.suite.pop()
        self.suite_attr.pop()

    def _start_keyword(self, name, attrs):
        self.keywords.append(name)
        self.keywords_attr.append(attrs)

    def _end_keyword(self, name, attrs):
        self.keywords.pop()
        self.keywords_attr.pop()

    def _html_decorate_header(self, priority):
        if priority == 1:
            return "<span style='color:red;font-weight: bold;'>[" + str(self.error_priority[priority]) + "]</span>"
        elif priority == 2:
            return "<span style='color:red;'>[" + str(self.error_priority[priority]) + "]</span>"
        elif priority == 3:
            return "<span style='color:#fed84f;'>[" + str(self.error_priority[priority]) + "]</span>"
        elif priority == 4:
            return "<span>[" + str(self.error_priority[priority]) + "]</span>"

    def _generate_div_with_btn(self, query_string, tracking, priority, error_exception):
        n = random.randint(1, 1000000)
        kw_tracking = ""
        for i, kw in enumerate(self.keywords):
            kw_tracking = kw_tracking + " -> " + kw + " (Line No: " + str(self.keywords_attr[i]["lineno"]) + ")"
        detail_msg = str(query_string) + "<p>Failed keywords tracking: {}</p>".format(kw_tracking)
        div_id = "div-" + str(n)
        btn_id = "btn-" + str(n)
        button_script = f"<button id={btn_id} onclick='toggleMoreInfo(\"{div_id}\",\"{btn_id}\")'>Expand details</button>"
        msg = f"{self._html_decorate_header(priority)} <b> {tracking} \nError {error_exception} </b> {button_script} <div id={div_id} style='display: none;'> {detail_msg} </div>"
        return msg

    def test(self, db):
        """ Connect to MySQL database """
        conn = None
        try:
            db_config = read_db_config(BuiltIn().get_variable_value("${env}"), db)
            conn = MySQLConnection(**db_config)
            if conn.is_connected():
                logger.console('connection pass:'+db)
                return True
            else:
                logger.console('connection failed:'+db)
                return False
        except Error as e:
            print(e)
        finally:
            if conn is not None:
                conn.close()

    def execute(self, db, query_string):
        """ Execute MySQL statement to MySQL database """
        conn = None
        if query_string.upper().startswith("DELETE") and " WHERE " not in query_string.upper():
            raise Exception("Don't use DELETE statement without WHERE clause. It deletes the whole table.")
        try:
            db_config = read_db_config(BuiltIn().get_variable_value("${env}"), db)
            conn = MySQLConnection(raise_on_warnings=False, **db_config)
            if conn.is_connected():
                cur = conn.cursor()
                cur.execute(query_string)
                affected_rows = cur.rowcount
                logger.info("Affected Rows = {}".format(affected_rows))
                if affected_rows == 0:
                    priority = 4
                    msg = "{0} {1} does not affect any rows.".format(self._html_decorate_header(priority), query_string)
                    logger.warn(msg, html=True)
                conn.commit()
            else:
                raise Exception('Connection failed.')

        except Exception as e:
            suite_name = self.suite[0] if len(self.suite) > 0 else None
            test_case_name = BuiltIn().get_variable_value("${TEST NAME}")
            if test_case_name is None:
                if self.keywords_attr[0]["type"] == "SETUP":
                    tracking = "In SUITE SETUP of suite: {}".format(suite_name)
                elif self.keywords_attr[0]["type"] == "TEARDOWN":
                    tracking = "In SUITE TEARDOWN of suite: {}".format(suite_name)
                else:
                    tracking = None
            else:
                test_case_number = test_case_name.split(maxsplit=1)[0]
                if self.keywords_attr[0]["type"] == "SETUP":
                    tracking = "In TEST SETUP of test case: {}".format(test_case_number)
                elif self.keywords_attr[0]["type"] == "TEARDOWN":
                    tracking = "In TEST TEARDOWN of test case: {}".format(test_case_number)
                else:
                    tracking = "In test case: {}".format(test_case_number)

            priority = 1
            msg = self._generate_div_with_btn(query_string, tracking, priority, e)
            script = "<script type='text/javascript'>function toggleMoreInfo(div_id, btn_id) { var detail = document.getElementById(div_id); if (detail.style.display === 'none') { detail.style.display = 'block'; document.getElementById(btn_id).innerHTML='Collapse details'; } else { detail.style.display = 'none'; document.getElementById(btn_id).innerHTML='Expand details'; } }</script>"
            logger.warn(msg + script, html=True)

        finally:
            if conn is not None:
                conn.cursor().close()
                conn.close()

    def query(self, db, query_string):
        """ Connect to MySQL database """
        conn = None
        try:
            db_config = read_db_config(BuiltIn().get_variable_value("${env}"), db)
            conn = MySQLConnection(**db_config)
            if conn.is_connected():
                cur = conn.cursor()
                cur.execute(query_string)
                return self.get_cur_with_description(cur)
            else:
                logger.console('connection failed.')
        except Error as e:
            print(e)
        finally:
            if conn is not None:
                conn.close()

    def get_cur_with_description(self, cur):
        data = []
        desList = self.fields(cur);
        for row in cur.fetchall():
            data_row = {};
            colNo=0
            for col in row:
                data_row[desList[colNo]] = row[colNo]
                colNo = colNo + 1
            data.append(data_row)
        return data;

    def fields(self, cursor):
        """ Given a DB API 2.0 cursor object that has been executed, returns
        a dictionary that maps each field name to a column index; 0 and up. """
        results = {}
        column = 0
        for d in cursor.description:
            results[column] = d[0]
            column = column + 1
        return results
