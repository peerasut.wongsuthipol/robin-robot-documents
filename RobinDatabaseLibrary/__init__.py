from __future__ import absolute_import

from .RobinDatabaseUtils import RobinDatabaseUtils

class RobinDatabaseLibrary(RobinDatabaseUtils):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'