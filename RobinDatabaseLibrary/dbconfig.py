import base64
from configparser import ConfigParser
from io import StringIO


def read_db_config(env='', db_name=''):
    db = {}
    if (env == 'dev' or env == 'dev2' or env == 'qa' or env == 'local'):
        # db_host_config = {
        #     'robin-host': 'rbh-' + env.lower() + '-aurora-db.rbhdev.internal'
        # }
        # db_name_config = {
        #     'payment': 'payment',
        #     'customer-profile': 'customer-profile',
        #     'order': 'order',
        #     'merchant': 'merchant',
        #     'notification': 'notification'
        # }
        
        if db_name is None :
            raise Exception('db name {0} not found in config'.format(db_name))
        else:
            if (env == 'dev' or env == 'dev2'):
                db['host'] = 'rbh-dev-aurora-db.rbhdev.internal'
                db['port']= '3306'
                if (db_name == 'promotion_engine'):
                    db['host'] = 'rbh-dev-promo-engine-mysql-db.rbhdev.internal'
                    db['port']= '3307'
                    
                db['user'] = 'dev'
                db['password'] = 'rbhdev'
                if env == 'dev':
                    db['database'] = db_name
                else: 
                    db['database'] = db_name + '_' + env
            elif env == 'qa':
                if (db_name == 'promotion_engine'):
                    db['host'] = 'rbh-' + env.lower() + '-promo-engine-mysql-db.rbhdev.internal'
                    db['port']= '3307'
                    db['database'] = db_name
                    db['user'] = 'dev'
                    db['password'] = 'rbhdev'
                elif (db_name == 'notification' or db_name == 'ota' or db_name == 'otp'):
                    db['host'] = 'rbh-' + env.lower() + '-mysql-db.rbhdev.internal'
                    db['port']= '3307'
                    db['database'] = db_name
                    db['user'] = 'robot'
                    db['password'] = 'c5w9lU4jW2xA8pqb'
                else:
                    db['host'] = 'rbh-' + env.lower() + '-aurora-db.rbhdev.internal'
                    db['port']= '3306'
                    db['database'] = db_name
                    db['user'] = 'dev'
                    db['password'] = 'rbhdev'
            elif env == 'local':
                db['host'] = 'rbh-' + env.lower() + '-aurora-db.rbhdev.internal'
                db['port']= '3306'
                if (db_name == 'promotion_engine'):
                    db['host'] = 'rbh-' + env.lower() + '-promo-engine-mysql-db.rbhdev.internal'
                    db['port']= '3307'

                db['database'] = db_name
                db['host'] = 'localhost'
                db['user'] = 'root'
                db['password'] = 'password'
            db['auth_plugin'] = 'mysql_native_password'
            db['ssl_disabled'] = True
        
        # db['password']= base64.b64decode('Um9ib3QxMjM0IQ==')
    else:
        raise Exception('env {0} not found in config'.format(env))

    return db
