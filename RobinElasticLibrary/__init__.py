from __future__ import absolute_import

from .RobinElasticUtils import RobinElasticUtils


class RobinElasticLibrary(RobinElasticUtils):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
