import json
import time

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from robot.libraries.BuiltIn import BuiltIn

from RobinElasticLibrary.elasticconfig import read_elastic_config


class RobinElasticUtils:
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

    def search_audit_by_correlationid(self, correlation_id):
        config = read_elastic_config(BuiltIn().get_variable_value("${env}"))
        payload = {
            "query": {
                "match": {
                    "CORRELATION_ID": correlation_id
                }
            }
        }

        headers = {
            'Content-Type': 'application/json',
            'Authorization': config['authorization']
        }

        limit_loop = 0
        while True:
            time.sleep(3)
            response = requests.post(config['host'] + config['log_audit_search_uri'], data=json.dumps(payload),
                                     headers=headers)
            response_json = response.json()
            if response_json['hits']['total']['value'] != 0:
                index = response.json()
                item = index['hits']['hits'][0]
                return item['_source']
            else:
                if limit_loop > 3:
                    index = response.json()
                    item = index['hits']['hits'][0]
                    return item['_source']
                else:
                    limit_loop = limit_loop + 1

    def search_bycorrelation_id_and_event_code(self, correlation_id, event_code):

        config = read_elastic_config(BuiltIn().get_variable_value("${env}"))

        payload = {
            "sort": [
                {
                    "@timestamp": {
                        "order": "asc"
                    }
                }
            ],
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "CORRELATION_ID": correlation_id
                            }
                        },
                        {
                            "match": {
                                "EVENT_CODE": event_code
                            }
                        }
                    ]
                }
            }
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': config['authorization']
        }
        limit_loop = 0
        while True:
            time.sleep(3)
            response = requests.post(config['host'] + config['log_audit_search_uri'], data=json.dumps(payload),
                                     headers=headers)
            response_json = response.json()
            if response_json['hits']['total']['value'] == 1:
                index = response.json()
                item = index['hits']['hits'][0]
                return item['_source']
            elif response_json['hits']['total']['value'] > 1:
                index = response.json()
                items = []
                for i in range(response_json['hits']['total']['value']):
                    item = index['hits']['hits'][i]['_source']
                    items.append(item)
                return items
            else:
                if limit_loop > 3:
                    raise Exception('Not found any audit log')
                else:
                    limit_loop = limit_loop + 1

    def search_bycorrelation_id_and_event_code_709(self, correlation_id, event_code, card):

        config = read_elastic_config(BuiltIn().get_variable_value("${env}"))

        payload = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "CORRELATION_ID": correlation_id
                            }
                        },
                        {
                            "match": {
                                "EVENT_CODE": event_code
                            }
                        },
                        {
                            "match": {
                                "CARD_MASK": card
                            }
                        }
                    ]
                }
            }
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': config['authorization']
        }

        limit_loop = 0
        while True:
            time.sleep(3)
            response = requests.post(config['host'] + config['log_audit_search_uri'], data=json.dumps(payload),
                                     headers=headers)
            response_json = response.json()
            if response_json['hits']['total']['value'] != 0:
                index = response.json()
                item = index['hits']['hits'][0]
                return item['_source']
            else:
                if limit_loop > 3:
                    index = response.json()
                    item = index['hits']['hits'][0]
                    return item['_source']
                else:
                    limit_loop = limit_loop + 1

    def search_audit_by_deliveryid(self, delivery_id):
        config = read_elastic_config(BuiltIn().get_variable_value("${env}"))
        payload = {
            "query": {
                "match": {
                    "DELIVERY_ID": delivery_id
                }
            }
        }

        headers = {
            'Content-Type': 'application/json',
            'Authorization': config['authorization']
        }

        limit_loop = 0
        while True:
            time.sleep(3)
            response = requests.post(config['host'] + config['log_audit_search_uri'], data=json.dumps(payload),
                                     headers=headers)
            response_json = response.json()
            if response_json['hits']['total']['value'] != 0:
                index = response.json()
                item = index['hits']['hits'][0]
                return item['_source']
            else:
                if limit_loop > 3:
                    index = response.json()
                    item = index['hits']['hits'][0]
                    return item['_source']
                else:
                    limit_loop = limit_loop + 1

    def search_queued_request_body_with_correlation_id(self, elastic_order_queue_uri, correlation_id):
        config = read_elastic_config(BuiltIn().get_variable_value("${env}"))
        payload = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "correlationId": correlation_id
                            }
                        },
                        {
                            "match_phrase": {
                                "uri": elastic_order_queue_uri
                            }
                        }
                    ]
                }
            }
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': config['authorization']
        }

        response = requests.post(config['host'] + config['log_request_search_uri'], headers=headers,
                                 data=json.dumps(payload))
        index = response.json()
        item = index['hits']['hits'][0]
        return item['_source']['reqBody']

    def search_response_body_with_correlationId_and_uri(self, uri: str, correlation_id: str, find_mocked_req=False):
        """Returns response body as dictionary-like object filtered by correlationId and uri. Can use for both mocked request and direct request.

        :param uri: just the uri e.g. /v1/search/merchant please include a slash in front of it.
                    If the uri has query paramter, please include the parameter as usually e.g. /v1/customer/search/history?service=HOME_LANDING
        :param correlation_id: correlationId of the request
        :param find_mocked_req: False by default. If true, then it automatically sets up prefix for the uri of mocked request
        :return: response body as an object
        """
        config = read_elastic_config(BuiltIn().get_variable_value("${ENV}"))
        if find_mocked_req:
            # Use question mark wildcard (?) to match one character for port number (4 chars use 4 question marks) of mocked request uri
            # Visit https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-wildcard-query.html
            uri = "http://jenkins-slave-mock-01.rbhdev.internal:????" + uri
        payload = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "correlationId": correlation_id
                            }
                        },
                        {
                            "wildcard": {
                                # field uri here need to use keyword as its analyzer otherwise, it treats slash (/) as a separator
                                "uri.keyword": {
                                    "value": uri
                                }
                            }
                        }
                    ]
                }
            }
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': config['authorization']
        }

        response = requests.post(config['host'] + config['log_request_search_uri'], headers=headers,
                                 data=json.dumps(payload))
        index = response.json()
        no_of_matched_req = index['hits']['total']['value']
        if no_of_matched_req == 0:
            raise Exception('Not found any request matching the given uri and correlationId')
        elif no_of_matched_req == 1:
            item = index['hits']['hits'][0]
            return json.loads(item['_source']['respBody'])
        else:
            raise Exception('Found multiple requests matching the given uri and correlationId')


    def search_value_in_request_body(self, elastic_order_queue_uri, value):
        config = read_elastic_config(BuiltIn().get_variable_value("${env}"))
        payload = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                    "reqBody": value
                            }
                        },
                        {
                            "match_phrase": {
                                "uri": elastic_order_queue_uri
                            }
                        }
                    ]
                }
            }
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': config['authorization']
        }

        response = requests.post(config['host'] + config['log_request_search_uri'], headers=headers,
                                 data=json.dumps(payload))
        index = response.json()
        item = index['hits']['hits'][0]
        return item['_source']['reqBody']
