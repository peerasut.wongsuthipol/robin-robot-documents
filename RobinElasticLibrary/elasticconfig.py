from configparser import ConfigParser


def read_elastic_config(env=''):
    # create parser and read ini configuration file
    s_config = """
    [dev]
    host = https://vpc-rbh-nonprod-elasticsearch-hm35avzpbobu3fw44qwhynwam4.ap-southeast-1.es.amazonaws.com
    authorization = Basic ZXNhZG1pbjpBZG1pbkByYmgxMjM=
    log_audit_search_uri = /rbh-log-dev-audit-*/_search
    log_request_search_uri = /rbh-log-dev-request-*/_search
    [dev2]
    host = https://vpc-rbh-nonprod-elasticsearch-hm35avzpbobu3fw44qwhynwam4.ap-southeast-1.es.amazonaws.com
    authorization = Basic ZXNhZG1pbjpBZG1pbkByYmgxMjM=
    log_audit_search_uri = /rbh-log-dev2-audit-*/_search
    log_request_search_uri = /rbh-log-dev2-request-*/_search
    [qa]
    host = https://vpc-rbh-nonprod-elasticsearch-hm35avzpbobu3fw44qwhynwam4.ap-southeast-1.es.amazonaws.com
    authorization = Basic ZXNhZG1pbjpBZG1pbkByYmgxMjM=
    log_audit_search_uri = /rbh-log-qa-audit-*/_search
    log_request_search_uri = /rbh-log-qa-request-*/_search
    """
    parser = ConfigParser()
    parser.read_string(s_config)
    elastic = {}
    section = env
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            elastic[item[0]] = item[1]
    else:
        raise Exception('{0} not found in file{1}'.format(section, parser.items))
    return elastic
