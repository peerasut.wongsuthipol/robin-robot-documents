from __future__ import absolute_import

from .RobinAWSUtils import RobinAWSUtils


class RobinAWSLibrary(RobinAWSUtils):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
