import json
from robot.api import logger
from awsconfig import read_aws_config
from robot.libraries.BuiltIn import BuiltIn
import boto3


class RobinAWSUtils:
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    # def __init__(self):
    #     self.awsConfig = read_aws_config(BuiltIn().get_variable_value("${env}"))

    def createAuthenToken(self):
        client = boto3.client('cognito-idp',
                              region_name=self.awsConfig['aws_region'],
                              aws_access_key_id=self.awsConfig['aws_accesskey'],
                              aws_secret_access_key=self.awsConfig['aws_secretkey']
                              )

    def sqs_send_message(self, sqs_name, message):
        sqs = boto3.client(
            'sqs',
            region_name=self.awsConfig['aws_region'],
            aws_access_key_id=self.awsConfig['aws_accesskey'],
            aws_secret_access_key=self.awsConfig['aws_secretkey'],
        )
        queue_url = self.awsConfig['sqs_base_url'] + sqs_name
        response = sqs.send_message(
            QueueUrl=queue_url,
            MessageBody=json.dumps(message)
        )
        return response['MessageId']

    def s3_put_object(self, bucket, path, data):
        # exampple for request 
        # bucker: 's3-dev-landing'
        # path: '/template/rbh-dev-template-v1.json'
        # data: data in json format or file data
        s3 = boto3.client(
            's3',
            region_name=self.awsConfig['aws_region'],
            aws_access_key_id=self.awsConfig['aws_accesskey'],
            aws_secret_access_key=self.awsConfig['aws_secretkey']
        )
        response = s3.put_object(
            Bucket=bucket,
            Body=json.dumps(data),
            Key=path,
            ContentType='application/json; charset=utf-8',
        )
        return response

    def s3_put(self, bucket, path, data):
        s3 = boto3.client(
            's3',
            region_name=self.awsConfig['aws_region'],
            aws_access_key_id=self.awsConfig['aws_accesskey'],
            aws_secret_access_key=self.awsConfig['aws_secretkey']
        )
        response = s3.put_object(
            Bucket=bucket,
            Body=data,
            Key=path
        )
        return response

    def s3_upload_file(self, filename, bucket, path):
        s3 = boto3.client(
            's3',
            region_name=self.awsConfig['aws_region'],
            aws_access_key_id=self.awsConfig['aws_accesskey'],
            aws_secret_access_key=self.awsConfig['aws_secretkey']
        )
        s3.upload_file(
            Filename=filename,
            Bucket=bucket,
            Key=path
        )

    def s3_copy(self, bucket_source, path_source, bucket_to, path_to):
        s3 = boto3.client(
            's3',
            region_name=self.awsConfig['aws_region'],
            aws_access_key_id=self.awsConfig['aws_accesskey'],
            aws_secret_access_key=self.awsConfig['aws_secretkey']
        )
        copy_source = {
            'Bucket': bucket_source,
            'Key': path_source
        }

        s3.copy(
            CopySource=copy_source,
            Bucket=bucket_to,
            Key=path_to
        )

    def s3_get_object(self, bucket, path):
        # exampple for request 
        # bucker: 's3-dev-landing'
        # path: '/template/rbh-dev-template-v1.json'
        s3 = boto3.client(
            's3',
            region_name=self.awsConfig['aws_region'],
            aws_access_key_id=self.awsConfig['aws_accesskey'],
            aws_secret_access_key=self.awsConfig['aws_secretkey']
        )
        result = s3.get_object(
            Bucket=bucket,
            Key=path,
        )
        data = result['Body'].read().decode('utf-8')
        return json.loads(data)
