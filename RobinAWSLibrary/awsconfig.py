from configparser import ConfigParser
from io import StringIO


def read_aws_config(env=''):
    # create parser and read ini configuration file
    s_config = """
    [dev]
    cognito_user_pool_id=ap-southeast-1_KV1wIKT2l
    cognito_client_id=668l4bscv3u353j8g23f3aqf73
    cognito_client_secret=1idqase55l5r66kaarg0pum96n3bmcndojctsqbi24ooqcdsrfvo
    aws_region=ap-southeast-1
    aws_accesskey=AKIA36UQ44657PJKKM7R
    aws_secretkey=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
    sqs_base_url=https://sqs.ap-southeast-1.amazonaws.com/821716379579/
    [dev2]
    cognito_user_pool_id=ap-southeast-1_KV1wIKT2l
    cognito_client_id=668l4bscv3u353j8g23f3aqf73
    cognito_client_secret=1idqase55l5r66kaarg0pum96n3bmcndojctsqbi24ooqcdsrfvo
    aws_region=ap-southeast-1
    aws_accesskey=AKIA36UQ44657PJKKM7R
    aws_secretkey=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
    sqs_base_url=https://sqs.ap-southeast-1.amazonaws.com/821716379579/
    [qa]
    cognito_user_pool_id=ap-southeast-1_eN3nWgIDW
    cognito_client_id=1jh5lm2nbqpqqou1t5e96c7frr
    cognito_client_secret=9ueavb4pgocu9gahm2c1is8lotjpt7s936n54svf1j3gtgn0eoi
    aws_region=ap-southeast-1
    aws_accesskey=AKIA36UQ44657PJKKM7R
    aws_secretkey=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
    sqs_base_url=https://sqs.ap-southeast-1.amazonaws.com/821716379579/
    """
    parser = ConfigParser()
    parser.read_string(s_config)
    aws = {}
    section = env
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            aws[item[0]] = item[1]
    else:
        raise Exception('{0} not found in file{1}'.format(section, parser.items))
    return aws
